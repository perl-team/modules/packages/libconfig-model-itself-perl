Source: libconfig-model-itself-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Dominique Dumont <dod@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl,
               xauth <!nocheck>,
               xvfb <!nocheck>
Build-Depends-Indep: cme <!nocheck>,
                     dh-sequence-bash-completion,
                     libapp-cmd-perl <!nocheck>,
                     libconfig-model-perl (>= 2.142),
                     libconfig-model-tester-perl <!nocheck>,
                     libconfig-model-tkui-perl <!nocheck>,
                     libdata-compare-perl <!nocheck>,
                     libfile-copy-recursive-perl <!nocheck>,
                     liblog-log4perl-perl <!nocheck>,
                     libpath-tiny-perl <!nocheck>,
                     libpod-pom-perl <!nocheck>,
                     libtest-differences-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-file-contents-perl <!nocheck>,
                     libtest-memory-cycle-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libtext-diff-perl <!nocheck>,
                     libyaml-libyaml-perl <!nocheck>
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libconfig-model-itself-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libconfig-model-itself-perl.git
Homepage: https://github.com/dod38fr/config-model/wiki
Rules-Requires-Root: no

Package: libconfig-model-itself-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         cme,
         libconfig-model-perl (>= 2.141),
         libconfig-model-tkui-perl (>= 1.370),
         libdata-compare-perl,
         liblog-log4perl-perl,
         libpath-tiny-perl,
         libpod-pom-perl,
         libyaml-libyaml-perl
Description: graphical model or schema editor for Config::Model
 Config::Model::Itself module is a plugin for cme which provides a
 Perl/Tk graphical interface to edit configuration models that are be
 used by Config::Model.
 .
 Config::Model::Itself also provides a model for Config::Model
 (hence the Itself name, you can also think of it as a meta-model).
 The model editor will use this meta-model to construct the graphical
 interface so you can edit the configuration model for *your*
 application. [ Config::Model::Itself is the "eat your own dog food" principle
 applied to Config::Model ;-) ]
 .
 Let's step back a little to explain. Any configuration data is, in
 essence, structured data. This data could be stored, for instance, in
 an XML file. A configuration model is a way to describe the structure
 and relation of all items of a configuration data set.
 .
 This configuration model is also expressed as structured data. This
 structure data is structured and follows a set of rules which are
 described for humans in Config::Model.
 .
 The structure and rules documented in Config::Model are also expressed
 in a model in the files provided with Config::Model::Itself.
 .
 Hence the possibity to verify, modify configuration data provided by
 Config::Model can also be applied on configuration models.
 .
 The model editor program launched with "cme meta edit"
